﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateOnAxis : MonoBehaviour
{
    public bool RandomSetting; 
    public float Speed;
    public bool RotateOnX;
    public bool RotateOnY;
    public bool RotateOnZ;
    public bool InvertRotation;
    
    // Start is called before the first frame update
    void Start()
    {
        if (RandomSetting)
        {
            Speed = Random.Range(10, 50);
            int random = Random.Range(0, 10);
            if (random > 5)
            {
                InvertRotation = true;
            }
            else
            {
                InvertRotation = false;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (InvertRotation)
        {
            Speed = -Mathf.Abs(Speed);
        }
        else
        {
            Speed = Mathf.Abs(Speed);
        }
        
        if (RotateOnX)
        {
            transform.Rotate(Vector3.right * Speed * Time.deltaTime);
        }
        if (RotateOnY)
        {
            transform.Rotate(Vector3.up * Speed * Time.deltaTime);
        }
        if (RotateOnZ)
        {
            transform.Rotate(Vector3.forward * Speed * Time.deltaTime);
        }
    }
}

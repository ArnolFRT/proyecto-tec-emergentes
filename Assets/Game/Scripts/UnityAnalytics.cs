using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;

public class UnityAnalytics : MonoBehaviour
{
    public static UnityAnalytics Instance;

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SendBasicEvent(string nameEvent)
    {
        AnalyticsResult result = Analytics.CustomEvent(nameEvent);
        Debug.Log("["+nameEvent+"]event state: "+result);
    }

    public void SendAdvancedEvent(string nameEvent, Dictionary<string, object> data)
    {
        AnalyticsResult result = Analytics.CustomEvent(nameEvent, data);
        Debug.Log("["+nameEvent+"]event state: "+result);
    }
}
